<?php
require_once '/var/www/html/custom/commons/lib/load-dolibarr-environnement.php';
$langs->loadLangs(array("conges_extra@conges_extra"));

llxHeader("", $langs->trans("HolidaysRecapPageName"));
print load_fiche_titre($langs->trans("HolidaysRecapPageName"), '', 'conges_extra.png@conges_extra');

require_once DOL_DOCUMENT_ROOT . '/holiday/class/holiday.class.php';
require_once DOL_DOCUMENT_ROOT . '/core/lib/date.lib.php';

include "../views/conges_month_summary.php";

require_once DOL_DOCUMENT_ROOT . '/custom/commons/lib/close-dolibarr-environnement.php';
?>