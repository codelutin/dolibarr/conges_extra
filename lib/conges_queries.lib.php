<?php

    function conges_fetchAllByMonth($db, $month, $year) {
        $sql = "SELECT";
        $sql .= " uu.lastname as user_lastname,";
        $sql .= " uu.firstname as user_firstname,";
        $sql .= " cp.fk_user as user_id,";
        $sql .= " cp.fk_type as type,";
        $sql .= " cp.date_debut,";
        $sql .= " cp.date_fin,";
        $sql .= " cp.halfday,";
        $sql .= " cp.ref,";
        $sql .= " cp.rowid as row_id,";
        $sql .= " cp.statut as status";
        $sql .= " FROM ".MAIN_DB_PREFIX."holiday as cp";
        $sql .= ", ".MAIN_DB_PREFIX."user as uu";
        $sql .= " WHERE cp.entity IN (".getEntity('holiday').")";
        $sql .= " AND cp.fk_user = uu.rowid ";
        $sql .= " AND ((";
        $sql .= "cp.date_debut BETWEEN '" . $year . "-" . $month . "-01 00:00:00' AND (date_trunc('month', '". $year . "-". $month . "-01'::date) + interval '1 month' - interval '1 day')::date " ;
        $sql .= "OR cp.date_fin BETWEEN '" . $year . "-" . $month . "-01 00:00:00' AND (date_trunc('month', '". $year . "-". $month . "-01'::date) + interval '1 month' - interval '1 day')::date " ;
        $sql .= ") OR ( ";
        $sql .= "cp.date_debut < '" . $year . "-" . $month . "-01 00:00:00' " ;
        $sql .= "AND cp.date_fin > '" . $year . "-" . $month . "-01 00:00:00' " ;
        $sql .= ") ) ";
        $sql .= "order by user_lastname , cp.date_debut";

        $resql = $db->query($sql);
        return $resql;
    }
?>