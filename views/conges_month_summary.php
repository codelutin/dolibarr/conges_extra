<?php
    /*
    * View
    */

    $action = GETPOST('action', 'aZ09');
    $massaction = GETPOST('massaction', 'alpha'); // The bulk action (combo box choice into lists)
    $toselect   = GETPOST('toselect', 'array'); // Array of ids of elements selected into a list
    $confirm = GETPOST('confirm', 'no');

    $display_month = GETPOST('month', 'int');
    $display_year = GETPOST('year', 'int');

    $form = new Form($db);
    $holidaystatic = new Holiday($db);
    $typeleaves = $holidaystatic->getTypes(1, -1);

	// Mass actions
	$objectclass = 'Holiday';
	$objectlabel = 'Holiday';
	$permissiontoread = $user->rights->holiday->read;
	$permissiontodelete = $user->rights->holiday->delete;
	$permissiontoapprove = $user->rights->holiday->approve;
	$uploaddir = $conf->holiday->dir_output;
	include DOL_DOCUMENT_ROOT.'/core/actions_massactions.inc.php';   

    // Security check
    if (! $user->rights->codelutin->myobject->read) {
        accessforbidden();
    }

    require_once '../lib/conges_queries.lib.php';
    require_once DOL_DOCUMENT_ROOT . '/custom/codelutin/lib/codelutin_ca_utils.php';
    if ($display_month == null || $display_year == null) {
        $display_month = date('m');
        $display_year = date('Y');
    }
    $resql = conges_fetchAllByMonth($db, $display_month, $display_year);

    if (!$resql) {
        dol_print_error($db);
        exit;
    } else {
        $previous_month_m = date('m', strtotime("-1 month", strtotime('01.'.$display_month .'.'. $display_year)));
        $previous_month_y = date('Y', strtotime("-1 month", strtotime('01.'.$display_month .'.'. $display_year)));
        $next_month_m = date('m', strtotime("+1 month", strtotime('01.'.$display_month .'.'. $display_year)));
        $next_month_y = date('Y', strtotime("+1 month", strtotime('01.'.$display_month .'.'. $display_year)));

        print '<div class="month-selector">'
            . '<a href="'.$_SERVER["PHP_SELF"].'?year='.$previous_month_y.'&month='.$previous_month_m.'">&#8592;&nbsp;</a>'
            . displayMonth($display_month) . ' ' . $display_year
            . '<a href="'.$_SERVER["PHP_SELF"].'?year='.$next_month_y.'&month='.$next_month_m.'">&nbsp&#8594;</a>'
            . '</div>';

        $num = $db->num_rows($resql);
        if (!$num) {
            print '<br/><p>' . $langs->trans('NoPendingRequests') . '</p>';
        }

        $i = 0;
        $currentEmployee = '';
        while ($i < $num) {
            $obj = $db->fetch_object($resql);
            if ($obj->user_id != $currentEmployee) {
                if ($currentEmployee != '') {
                    print '</tbody></table></form>';
                }
                $currentEmployee = $obj->user_id;
                print '<h3 style="margin-top: 40px; margin-bottom: 0;">'.  $obj->user_firstname . ' ' . $obj->user_lastname . '</h3>';
                $action = "document.getElementById('approve-all-hollidays-for-user-" . $obj->user_id . "-confirm').style.display = 'inherit'";
                print '<a onclick="' . $action . '" style="float: right; margin-bottom: 5px;">' . $langs->trans('ApprovePendingRequests') . '</a>';
                print '<form id="approve-all-hollidays-for-user-' . $obj->user_id . '" action="'.$_SERVER["PHP_SELF"].'?year='.$display_year.'&month='.$display_month.'" method="POST">'."\n";
                print '<div id="approve-all-hollidays-for-user-' . $obj->user_id . '-confirm" style="display: none;">'
                    . $form->formconfirm(
                        $_SERVER["PHP_SELF"],
                        $langs->trans("ConfirmMontEmployeeAbsencesApproval"),
                        $langs->trans("ConfirmMontEmployeeAbsencesApprovalQuestion", $obj->user_firstname . ' ' . $obj->user_lastname ),
                        "approveleave", null, 'yes', 0, 200, 500, 1)
                    . '</div>';
                print '<table class="liste"><tbody>';
            }

            $date_debut_precision = '';
            $date_fin_precision = '';
			if ($obj->halfday == 2) {
				$date_debut_precision = ' après-midi ';
                $date_fin_precision = ' matin ';
			} elseif ($obj->halfday == 1) {
				$date_fin_precision = ' matin ';
			} elseif ($obj->halfday == -1) {
				$date_debut_precision = ' après-midi ';
			}
            $nbopenedday = num_open_day($db->jdate($obj->date_debut, 1), $db->jdate($obj->date_fin, 1), 0, 1, $obj->halfday);	// user jdate(..., 1) because num_open_day need UTC dates
            

			// Leave request
			$holidaystatic->id = $obj->row_id;
			$holidaystatic->ref = ($obj->ref ? $obj->ref : $obj->row_id);
			$holidaystatic->statut = $obj->status;
			$holidaystatic->date_debut = $db->jdate($obj->date_debut);
            $url = DOL_URL_ROOT.'/holiday/card.php?id=' . $obj->row_id;

            $type = $typeleaves[$obj->type];

            if ($obj->status == $holidaystatic::STATUS_VALIDATED) {
                print '<input type="hidden" name="toselect[]" value=' . $obj->row_id . '>';
            }

            if ($obj->date_debut == $obj->date_fin) {
                print '<tr class="oddeven">'
                    . '<td class="width-150">'
                    . ($langs->trans($type['code']) != $type['code'] ? $langs->trans($type['code']) : $type['label'])
                    . '</td><td class="width-50">'
                    . $nbopenedday . ' j'
                    . '</td><td>'
                        . '<a href="'. $url . '">'
                        . 'Le '
                        . dol_print_date($db->jdate($obj->date_debut), '%A %d %B') . $date_debut_precision . ' ' . $date_fin_precision
                        . '</a>'
                    . '</td><td class="width-150">'
                        . $holidaystatic->getLibStatut(5)
                    . '</td></tr>';
            } else {
                print '<tr class="oddeven">'
                    . '<td class="width-150">'
                    . ($langs->trans($type['code']) != $type['code'] ? $langs->trans($type['code']) : $type['label'])
                    . '</td><td class="width-50">'
                    . $nbopenedday . ' j'
                    . '</td><td>'
                        . '<a href="'. $url . '">'
                        . 'Du '
                        . dol_print_date($db->jdate($obj->date_debut), '%A %d %B') . $date_debut_precision
                        . ' au '
                        . dol_print_date($db->jdate($obj->date_fin), '%A %d %B') . $date_fin_precision
                        . '</a>'
                    . '</td><td class="width-150">'
                        . $holidaystatic->getLibStatut(5)
                    . '</td></tr>';
                
            }
            $i++;
        }

        if ($currentEmployee != '') {
            print '</tbody></table></form>';
        }
    }
?>