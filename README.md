#  🇬🇧 CONGES_EXTRA MODULE FOR [DOLIBARR ERP CRM](https://www.dolibarr.org)

## Features

A set of additional behaviours and screens for the RH dolibarr module (e.g. monthly recap of taken vacations).

---

# 🇫🇷 MODULE CONGES_EXTRA POUR L'[ERP DOLIBARR](https://www.dolibarr.org)

## Fonctionnalités

Ce module regroupe des fonctionnalités et des écrans supplémentaires pour le module RH dolibarr (par exemple, récapitulation mensuelle des vacances prises).
